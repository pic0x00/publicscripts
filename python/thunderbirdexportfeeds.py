"""
This script help to make tree folder view during import feeds from google reader to thunderbird.
Issue:
https://stackoverflow.com/questions/15483824/where-is-the-folder-view-for-rss-feeds-in-thunderbird
"""

from bs4 import BeautifulSoup
temporarylist=[]
outlist=[]
EXTFILEPATHIN="PATH TO XML FILES"
OUTFILEPATH="PATH TO OUT XML FILE"

def stringproccessing(filepath):
    with open(filepath,encoding="utf8") as file:
        temporarylist=file.readlines()
    for tempstring in temporarylist:
        if ("htmlUrl" in tempstring):
            temp1=BeautifulSoup(tempstring,"xml")
            temp2=temp1.outline
            tempstring="<outline"+" "+"title="+"\""+temp2["title"]+"\""+" "+"icon=\"\">"+tempstring.strip()+"</outline>"
            outlist.append(tempstring)
        else: outlist.append(tempstring)
    return ("".join(outlist)).strip()

with open(OUTFILEPATH,"w",encoding="utf8") as xmlout:
    soup=BeautifulSoup(stringproccessing(EXTFILEPATHIN),"xml")
    xmlout.write(soup.prettify())
    xmlout.close()
